package com.calc.ferdous.webcalc;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {


    Button btnclick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        boolean isConnected =false;

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
         isConnected= activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();




        if(isConnected==true ){

                Toast.makeText(this,"Internet Connected",Toast.LENGTH_LONG).show();

            }

            else{

                Toast.makeText(this," No Internet Connection",Toast.LENGTH_LONG).show();

            }




         btnclick= (Button) findViewById(R.id.btnclick);

        btnclick.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {


                Intent webIntent = new Intent(getApplicationContext(),webActivity.class);
                startActivity(webIntent);
            }
        });



    }



}
