package com.calc.ferdous.webcalc;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebChromeClient;
import android.widget.Toast;

public class webActivity extends AppCompatActivity {

    WebView webvw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        webvw = (WebView) findViewById(R.id.webvw);

        //webvw.addJavascriptInterface(new WebAppInterface(this), "Android");

        webvw.getSettings().setJavaScriptEnabled(true);
        webvw.loadUrl("file:///android_asset/calculator.html");
        webvw.addJavascriptInterface(new WebAppInterface(this,webvw),"Android");



    }



}



class WebAppInterface {
    Context mContext;
    WebView webvw;

    int num1=0,num2=0,result=0;

    boolean isplus;
    boolean issub;
    boolean ismul;
    boolean isdiv;


    WebAppInterface(Context c,WebView p) {
        mContext = c;
        webvw = p;

        isplus=false;
         issub=false;
        ismul=false;
         isdiv=false;
    }


    private boolean checkNum(int p){

        if(num1==0 ){

            num1=p;
            return false;

        }

        return true;

    }



    @JavascriptInterface
    public String plus(String item) {

        int n = Integer.parseInt(item);

        isplus=true;
        issub=false;
        ismul=false;
        isdiv=false;

        if(checkNum(n)){

            num2=n;
            result=num1+num2;

            Toast.makeText(mContext, String.valueOf(result), Toast.LENGTH_SHORT).show();
            num1=0;
            num2=0;


           return String.valueOf(result);

           // webvw.loadUrl("javascript: {document.getElementsById('txt1').value ='"+String.valueOf(result)+"';};");

        }

        return "";
    }


    @JavascriptInterface
    public String sub(String item) {

        int n = Integer.parseInt(item);

        isplus=false;
        issub=true;
        ismul=false;
        isdiv=false;

        if(checkNum(n)){

            num2=n;
            result=num1-num2;

            Toast.makeText(mContext, String.valueOf(result), Toast.LENGTH_SHORT).show();
            num1=0;
            num2=0;


            return String.valueOf(result);

            // webvw.loadUrl("javascript: {document.getElementsById('txt1').value ='"+String.valueOf(result)+"';};");

        }

        return "";
    }


    @JavascriptInterface
    public String div(String item) {

        int n = Integer.parseInt(item);

        isplus=false;
        issub=false;
        ismul=false;
        isdiv=true;

        if(checkNum(n)){

            num2=n;
            result=num1/num2;

            Toast.makeText(mContext, String.valueOf(result), Toast.LENGTH_SHORT).show();
            num1=0;
            num2=0;


            return String.valueOf(result);

            // webvw.loadUrl("javascript: {document.getElementsById('txt1').value ='"+String.valueOf(result)+"';};");

        }

        return "";
    }


    @JavascriptInterface
    public String mul(String item) {

        int n = Integer.parseInt(item);

        isplus=false;
        issub=false;
        ismul=true;
        isdiv=false;

        if(checkNum(n)){

            num2=n;
            result=num1*num2;

            Toast.makeText(mContext, String.valueOf(result), Toast.LENGTH_SHORT).show();
            num1=0;
            num2=0;


            return String.valueOf(result);

            // webvw.loadUrl("javascript: {document.getElementsById('txt1').value ='"+String.valueOf(result)+"';};");

        }

        return "";
    }

    @JavascriptInterface
    public String equal(String item) {

        int n = Integer.parseInt(item);

        if(checkNum(n)){



            if(isplus==true && issub==false && ismul==false && isdiv==false){

                plus(item);
            }else if(isplus==false && issub==true && ismul==false && isdiv==false){

                sub(item);

            }else if(isplus==false && issub==false && ismul==false && isdiv==true){

                div(item);

            }else if(isplus==false && issub==false && ismul==true && isdiv==false){

                mul(item);

            }

            Toast.makeText(mContext, "Equal", Toast.LENGTH_SHORT).show();




            return String.valueOf(result);

            // webvw.loadUrl("javascript: {document.getElementsById('txt1').value ='"+String.valueOf(result)+"';};");

        }

        return String.valueOf(num1);
    }


}
